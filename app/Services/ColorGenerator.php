<?php
namespace App\Services;

class ColorGenerator
{
    public function getRandomAvatarColor() {
        return '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
    }
}
