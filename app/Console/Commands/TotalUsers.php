<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class TotalUsers extends Command
{
    protected $signature = 'user:count';

    protected $description = 'Display the number of users in the database';

    public function handle()
    {
        $count = DB::table('users')->count();
        $this->info("There are {$count} users in the database.");
    }
}
