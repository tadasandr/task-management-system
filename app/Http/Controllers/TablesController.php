<?php

namespace App\Http\Controllers;
use App\Models\Table;
use App\Models\Workspace;
use App\Models\Section;
use Illuminate\Http\Request;
use Inertia\Inertia;

class TablesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // finding user trying to create a table
        
        $workspace = Workspace::find($request->input('workspaceId'));

        // creating a table
        $table = new Table(['name'=>$request->input('name')]);
        $savedTable = $workspace->tables()->save($table);

        $section = new Section(['name' => 'Initial tasks']);
        $savedTable->sections()->save($section);

        return Inertia::render('Workspace', [
            'newTable' => $table
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $table = Table::find($id);
    
        return Inertia::render('Workspace',[
            'firstSections' => $table->sections
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
