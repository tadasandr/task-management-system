<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\Models\User;
use App\Models\Workspace;
use App\Models\Table;
use Inertia\Inertia;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index() {
        error_log('refetched');
        $userID = Auth::user()->id;
        $workspaces = Workspace::whereHas('users', function ($query) use ($userID) {
            $query->where('id', $userID);
         })
         ->select('id', 'name')
         ->with('tables')
         ->get();

         $firstUserWorkspace = Workspace::whereHas('users', function ($query) use ($userID) {
            $query->where('id', $userID);
        })
        ->select('id', 'name')
        ->with(['tables', 'tables.sections', 'tables.sections.tasks'])
        ->first();

        return Inertia::render('Dashboard', [
            'workspaces' => $workspaces,
            'firstUserWorkspace' => $firstUserWorkspace->load('users'), 
            'favicon' => asset('public\favicon.ico')
        ]);
    }
}
