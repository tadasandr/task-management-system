<?php

namespace App\Http\Controllers;
use App\Models\Workspace;
use App\Models\User;
use App\Models\Table;
use App\Models\Section;
use Inertia\Inertia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WorkspacesController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    public function store(Request $request)
    {
        // finding user trying to create a workspace
        $user = User::find($request->input('user_id'));
        // creating workspace
        $workspace = Workspace::create(['name'=>$request->input('name'), 
        'admins' => ['owner' => $request->input('user_id')], 
        'owner' => "{$request->input('user_id')}"]);
        // creating an initial table to workspace
        $table = new Table(['name' => $request->tableName]);
        // saving records to database
        $user->workspaces()->save($workspace);

        $savedTable = $workspace->tables()->save($table);

        $section = new Section(['name' => 'Initial tasks']);
        $savedTable->sections()->save($section);

        $workspace['tables'][0] = $table;

        return Inertia::render('Dashboard', [
            'newWorkspace' => $workspace
        ]);
    }

    public function show($id, $tableId)
    {
        $workspace = Workspace::where('id', $id)->with('tables')->with('users')->first();

        $sections = Table::find($tableId)->sections()->with('tasks.users')->get();

        return Inertia::render('Workspace', [
            'workspace' => $workspace,
            'sections' => $sections
        ]);
    }

    public function addUserToWorkspace(Request $request) {
        error_log("adding user");
        $workspace = Workspace::find($request->input('workspaceId'));
        $user = User::find($request->input('userId'));

        if($exists = $workspace->users->contains($user->id)) {
            error_log('exists');
        } else {
            $workspace->users()->save($user);
        }

        return Inertia::render('Dashboard', [
            'newWorkspace' => $workspace->load('tables')
        ]);
    }
}
