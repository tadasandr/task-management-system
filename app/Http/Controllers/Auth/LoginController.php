<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Inertia\Inertia;

class LoginController extends Controller
{

    public function create() {
        Inertia::share([
            'config' => function () {
                return [
                    'authModal' => true
                ];
            }
        ]);

        return Inertia::render('Authentication');
    }

    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        error_log("login ran in laravel");

        // Gauna login laukus in frontendo

        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
 
        // Auth::attempt megina prisijungti su tais credentials

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
 
            return redirect()->intended('/dashboard');
        }
    
        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ])->onlyInput('email');
    }

    // public function register(Request $request) {
    //     $attributes = $request->validate([
    //         'name' => ['required'],
    //         'email' => ['required', 'email'],
    //         'password' => ['required']
    //     ]);

    //     $attributes['password'] = Hash::make($attributes['password']);
    //     $attributes['avatar_color'] = $this->getRandomAvatarColor();

    //     User::create($attributes);

    //     if (Auth::attempt($attributes, true)) {
    //         error_log("registering and logging in");
    //         $request->session()->regenerate();
 
    //         return redirect()->intended('/dashboard');
    //     }
 
    //     return back()->withErrors([
    //         'email' => 'The provided credentials do not match our records.',
    //     ])->onlyInput('email');
    // }

    public function register(Request $request) {
        $attributes = $request->validate([
            'name' => ['required'],
            'email' => ['required', 'email'],
            'password' => ['required']
        ]);
    
        $attributes['password'] = Hash::make($attributes['password']);
        $attributes['avatar_color'] = $this->getRandomAvatarColor();
    
        $user = User::create($attributes);
    
        // Log the user in manually
        Auth::login($user);
    
        $request->session()->regenerate();
    
        return redirect()->intended('/dashboard');
    }

    public function logout() {
        Auth::logout();
        return redirect('authentication');
    }

    public function getRandomAvatarColor() {
        return '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
    }
}
