<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Section;
use Inertia\Inertia;
use App\Models\Task;


class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $section = Section::find($request->input('sectionId'));

        $task = Task::create([
            'title' => $request->input('title'),
            'type' => $request->input('type'),
            'description' => $request->input('description'),
            'status' => $request->input('status'),
            'due_date' => $request->input('due_date'),
            'relevance' => $request->input('relevance')
        ]);

        $section->tasks()->save($task);

        $users = $request->input('assigned_people');
        $userIds = [];

        // dd($users);

        foreach($users as $user) {
            array_push($userIds, $user['id']);
        }

        $task->users()->attach($userIds);
        
    
        return Inertia::render('Workspace', [
            'newTask' => $task->load('users')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::find(intval($id));

        $task->update([
            'type' => $request->input('type'),
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'status' => $request->input('status'),
            'due_date' => $request->input('due_date'),
            'relevance' => $request->input('relevance')
        ]);

        $users = $request->input('assigned_people');
        $userIds = [];

        // dd($users);

        foreach($users as $user) {
            array_push($userIds, $user['id']);
        }

        $task->users()->sync($userIds);
    }

    public function updateStatus(Request $request, $id)
    {
        $task = Task::find(intval($id));
        $task->update([
            'status' => $request->input('status')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::find($id);
        $task->delete();
        $task->users()->detach();

        // return Inertia::render('Workspace', [
        //     'success' => true
        // ]);
    }
}
