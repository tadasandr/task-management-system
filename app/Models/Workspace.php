<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Model;
use App\Traits\UUID;
use App\Models\Table;

class Workspace extends Model
{
    use HasFactory, UUID;

    protected $fillable = [
        'name',
        'admins',
        'owner',
    ];


    public function tables() {
        return $this->hasMany(Table::class);
    }

    // public function user() {
    //     return $this->belongsTo(User::class);
    // }


    protected $casts = [
        'admins' => 'json',
    ];

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }
}
