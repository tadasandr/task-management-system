<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected static function boot() {
        parent::boot();

        static::created(function ($model) {
            $model->load('users');
        });
    }

    protected $fillable = [
        'title',
        'type',
        'description',
        'due_date',
        'relevance',
        'status'
    ];

    public function section() {
        return $this->belongsTo(Section::class);
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }
}
