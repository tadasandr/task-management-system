<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use App\Traits\UUID;
use App\Observers\UserObserver;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, UUID;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

     protected $dispatchesEvents = [
        'created' => UserObserver::class,
        'updated' => UserObserver::class,
    ];

    protected $dates = [
        'last_login_date',
    ];

    protected $fillable = [
        'name',
        'email',
        'password',
        'avatar_color',
        'account_status',
        'last_login_date',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // public function workspaces() {
    //     return $this->hasMany(Workspace::class);
    // }

    public function isActive(\DateTime $currentDate, int $daysThreshold = 30): bool
    {
        if ($this->account_status !== 'active') {
            return false;
        }

        $lastActiveDate = (clone $currentDate)->modify("-{$daysThreshold} days");

        return $this->last_login_date->gte($lastActiveDate);
    }

//     public function isActive(DateTime $currentDate, int $daysThreshold = 30): bool
// {
//     if ($this->account_status !== 'active') {
//         return false;
//     }

//     $lastLoginDate = new DateTime($this->last_login_date);
//     $interval = $lastLoginDate->diff($currentDate);

//     return $interval->days <= $daysThreshold;
// }

    public function workspaces(): BelongsToMany
    {
        return $this->belongsToMany(Workspace::class);
    }

    public function tasks(): BelongsToMany
    {
        return $this->belongsToMany(Task::class);
    }
}
