<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id()->index();
            $table->integer('section_id')->unsigned()->nullable()->index();
            $table->string('type');
            $table->string('title');
            $table->mediumText('description');
            $table->string('status')->default('TODO');
            $table->date('due_date')->default(date("Y-m-d"));
            $table->string('relevance')->default('Normal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
};
