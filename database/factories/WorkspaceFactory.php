<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Workspace;
use App\Models\User;

class WorkspaceFactory extends Factory
{
    protected $model = Workspace::class;

    public function definition()
    {
        return [
            'name' => $this->faker->company,
            'owner' => User::factory(),
            'admins' => json_encode([]),
        ];
    }

    public function configure()
    {
        return $this->afterCreating(function (Workspace $workspace) {
            if ($workspace->owner) {
                $workspace->users()->attach($workspace->owner);
            }
        });
    }
}