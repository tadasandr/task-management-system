<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Section;
use App\Models\Task;


/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Section>
 */



class SectionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Section::class;

    public function definition()
    {
        return [
            'name' => $this->faker->words(2, true),
        ];
    }

    public function withTasks(int $count = 1)
    {
        return $this->has(Task::factory()->count($count));
    }
}
