<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Table;
use App\Models\Section;
use App\Models\Workspace;

/**
 * \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Table>
 */
class TableFactory extends Factory
{
    protected $model = Table::class;

    public function definition()
    {
        return [
            'name' => $this->faker->words(3, true),
            'workspace_id' => Workspace::factory(), // Add this line
        ];
    }

    public function withSections(int $count = 1)
    {
        return $this->has(Section::factory()->count($count));
    }
}