<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Task;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Task>
 */
class TaskFactory extends Factory
{
    protected $model = Task::class;

    public function definition()
    {
        return [
            'title' => $this->faker->sentence,
            'type' => $this->faker->word,
            'description' => $this->faker->paragraph,
            'due_date' => $this->faker->date,
            'relevance' => $this->faker->randomElement(['high', 'medium', 'low']),
            'status' => $this->faker->randomElement(['todo', 'in_progress', 'completed']),
        ];
    }
}
