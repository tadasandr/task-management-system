<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => "Tadas Andrijauskas",
            'email' => "tadasandrijauskass@gmail.com",
            "password" => Hash::make('tadas123'),
            "avatar_color" => $this->getRandomAvatarColor()
        ]);

        User::create([
            'name' => "Petras Petrauskas",
            'email' => "petraspetrauskas@gmail.com",
            "password" => Hash::make('petras123'),
            "avatar_color" => $this->getRandomAvatarColor()
        ]);

        User::create([
            'name' => "Onute Onaite",
            'email' => "onuteonaite@gmail.com",
            "password" => Hash::make('onute123'),
            "avatar_color" => $this->getRandomAvatarColor()
        ]);
    }

    public function getRandomAvatarColor() {
        return '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
    }
}
