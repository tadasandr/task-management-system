<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\WorkspacesController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\TablesController;
use App\Http\Controllers\SectionController;
use App\Http\Controllers\TaskController;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/dashboard');
});

Route::get('/authentication', [LoginController::class, 'create'])->name('authentication');
Route::post('/login', [LoginController::class, 'login'])->name('login');
Route::post('/register', [LoginController::class, 'register'])->name('register');

Route::middleware(['auth'])->group(function() {

    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::get('/chat', function () {
        return Inertia::render('Chat', [
        ]);
    })->name('chat');;
    
    
    Route::get('/team', function () {
        return Inertia::render('Teams', [
        ]);
    })->name('teams'); 

    Route::resource('/tasks', TaskController::class);
    Route::put('/tasks/status/{id}', [TaskController::class, 'updateStatus']);

    Route::resource('/sections', SectionController::class);

    // WORKSPACES

    Route::get('/workspaces/{workspace}/{table}', [WorkspacesController::class, 'show']);
    Route::post('/workspaces', [WorkspacesController::class, 'store']);

    Route::post('/invite', [WorkspacesController::class, 'addUserToWorkspace']);

    Route::resource('/tables', TablesController::class);

    // WORKSPACES END

    Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
});

