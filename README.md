<h2>Task Management System</h2>
<br/>
<p>Full-stack web application which I developed for my course project. The stack in this project includes: </p>
<ul>
<li>
Laravel
</li>
<li>
Vue.js 3
</li>
<li>
Inertia.js
</li>
<li>
MySQL (MariaDB)
</li>
</ul>
<p>Some of the features:</p>
<ul>
<li>Feature, unit tests</li>
<li>
Full authentication (registration/login)
</li>
<li>
Workspaces/tables/section/tasks CRUD & many-to-many, one-to-many relationships
</li> 
<li>
Interactive UI
</li>
<img src="https://gitlab.com/tadasandr/task-management-system/-/raw/main/resources/assets/tasksadded.png"/>
<li>
Gantt diagram (in-development)
</li>
<li>Assigning people to tasks</li>
<img src="https://gitlab.com/tadasandr/task-management-system/-/raw/main/resources/assets/tasksassignment.png"/>
<li>Changing tasks progress</li>
<li>Updating tasks</li>
<li>Filtering/Sorting/Searching</li>
<li>Joining and creating workspaces</li>
<img src="https://gitlab.com/tadasandr/task-management-system/-/raw/main/resources/assets/tasksjoining.png"/>
</ul>
<li>More coming soon :)</li>