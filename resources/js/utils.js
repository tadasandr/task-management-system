async function preserveUrl(callback) {
    const currentUrl = window.location.href;
    console.log("preservedUrl: " + currentUrl);
    let result;
    try {
        result = await callback();
    } catch (e) {
        console.log(e.message);
        console.log("replacing url: " + currentUrl);
        window.history.replaceState(null, null, currentUrl);
    }
    console.log("replacing url: " + currentUrl);
    window.history.replaceState(null, "", currentUrl);
    return result;
}

export default preserveUrl;
