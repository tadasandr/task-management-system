import { createApp, h } from "vue";
import { InertiaProgress } from "@inertiajs/progress";
import { createInertiaApp, Link } from "@inertiajs/vue3";
import { resolvePageComponent } from "laravel-vite-plugin/inertia-helpers";
import { createStore } from "vuex";
import "vuetify/styles";
import { createVuetify } from "vuetify";
import * as components from "vuetify/components";
import * as directives from "vuetify/directives";
import VueGanttastic from "@infectoone/vue-ganttastic";
import { library } from "@fortawesome/fontawesome-svg-core";

/* import font awesome icon component */
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import Favicon from "./Components/UI/Favicon.vue";

/* import specific icons */
import {
    faUserSecret,
    faHouse,
    faMagnifyingGlass,
    faBars,
    faPlus,
    faBell,
    faGear,
    faChartLine,
    faRectangleList,
    faComment,
    faUsers,
    faFolderOpen,
    faCaretUp,
    faCaretDown,
    faTable,
    faUserPlus,
    faExclamationCircle,
} from "@fortawesome/free-solid-svg-icons";
import VueAxios from "vue-axios";
import axios from "axios";

InertiaProgress.init();

// // components
// import ChatPage from "./components/chat/ChatPage.vue";
// import DashboardPage from "./components/dashboard/DashboardPage.vue";
// import ProjectBoard from "./components/projectboard/ProjectBoard.vue";
// import TeamMembers from "./components/teammembers/TeamMembers.vue";

/* add icons to the library */
library.add(
    faUserSecret,
    faHouse,
    faMagnifyingGlass,
    faBars,
    faPlus,
    faBell,
    faGear,
    faChartLine,
    faRectangleList,
    faComment,
    faUsers,
    faFolderOpen,
    faCaretUp,
    faCaretDown,
    faTable,
    faUserPlus,
    faExclamationCircle
);

const store = createStore({
    state() {
        return {
            workspaces: [],
            tables: [],
            sections: [],
            tasks: [],
            selectedWorkspace: {},
            selectedWorkspaceObj: {},
            selectedTable: {},
            tasksLoading: false,
            activeSection: 0,
            isTaskEditingMode: false,
            editingTask: null,
            searchString: "",
            filteredTaskList: [],
            isFilteringMode: false,
            usersInWorkspace: null,
        };
    },
    mutations: {
        // WORKSPACES ------------------------
        setSelectedWorkspace(state, payload) {
            state.selectedWorkspace = payload;
        },
        setSelectedWorkspaceObj(state, payload) {
            state.selectedWorkspaceObj = payload;
        },
        setWorkspaces(state, payload) {
            state.workspaces = payload;
        },
        setUsersInWorkspace(state, payload) {
            state.usersInWorkspace = payload;
        },
        // WORKSPACES END ------------------------
        // TABLES --------------------------------
        setTables(state, payload) {
            state.tables = payload;
        },
        setSelectedTable(state, payload) {
            state.selectedTable = payload;
        },
        addTableToTables(state, payload) {
            state.tables.push(payload);
        },
        // TABLES END ---------------------------
        // TASKS  -------------------------------
        setSections(state, payload) {
            state.sections = payload;
        },
        addTaskToSection(state, payload) {
            state.sections[state.activeSection.index].tasks.push(payload);
        },
        findSectionAndDelete(state, payload) {
            var foundSection = state.sections.findIndex((x) => x.id == payload);
            state.sections.splice(foundSection, 1);
        },
        findTaskAndUpdate(state, payload) {
            var foundSection = state.sections.findIndex(
                (x) => x.id == payload.sectionId
            );
            console.log("found section:" + foundSection);
            var foundTask = state.sections[foundSection].tasks.findIndex(
                (x) => x.id == payload.id
            );
            state.sections[foundSection].tasks[foundTask] = payload;
        },
        findStatusAndUpdate(state, payload) {
            var foundSection = state.sections.findIndex(
                (x) => x.id == payload.sectionId
            );
            console.log("found section:" + foundSection);
            var foundTask = state.sections[foundSection].tasks.findIndex(
                (x) => x.id == payload.id
            );
            state.sections[foundSection].tasks[foundTask].status =
                payload.status;
        },
        findTaskAndDelete(state, payload) {
            var foundSection = state.sections.findIndex(
                (x) => x.id == payload.sectionId
            );
            console.log("found section:" + foundSection);
            var foundTask = state.sections[foundSection].tasks.findIndex(
                (x) => x.id == payload.id
            );
            state.sections[foundSection].tasks.splice(foundTask, 1);
        },
        setTasks(state, payload) {
            state.tasks = payload;
        },
        setTasksLoading(state, payload) {
            state.tasksLoading = payload;
        },
        // TASKS END ----------------------------
        setActiveSection(state, payload) {
            state.activeSection = payload;
        },
        setIsTaskEditingMode(state, payload) {
            state.isTaskEditingMode = payload;
        },
        setEditingTask(state, payload) {
            state.editingTask = payload;
        },
        // SEARCH ------------------------------
        setSearchString(state, payload) {
            state.searchString = payload;
        },
        // SEARCH END --------------------------
        // FILTERING  --------------------------
        setFilteredTaskList(state, payload) {
            state.filteredTaskList = payload;
        },
        filterTaskList(state, payload) {
            state.filteredTaskList = state.sections.map((section) => ({
                ...section,
                tasks: section.tasks.filter((task) =>
                    task.users.find((user) => user.id === payload)
                ),
            }));
        },
        setFilteringMode(state, payload) {
            state.isFilteringMode = payload;
        },
        // FILTERING END -----------------------
        RESET_STATE(state) {
            Object.assign(state, getDefaultState());
        },
    },
    getters: {
        getUsersInWorkspace(state) {
            return state.usersInWorkspace;
        },
        getWorkspaces(state) {
            return state.workspaces;
        },
        getTables(state) {
            return state.tables;
        },
        getSections(state) {
            return state.sections;
        },
        getTasks(state) {
            return state.tasks;
        },
        getSelectedWorkspace(state) {
            return state.selectedWorkspace;
        },
        getSelectedTable(state) {
            return state.selectedTable;
        },
        getSelectedWorkspaceObj(state) {
            return state.selectedWorkspaceObj;
        },
        getTasksLoading(state) {
            return state.tasksLoading;
        },
        getActiveSection(state) {
            return state.activeSection;
        },
        getIsTaskEditingMode(state) {
            return state.isTaskEditingMode;
        },
        getEditingTask(state) {
            return state.editingTask;
        },
        getSearchString(state) {
            return state.searchString;
        },
        getFilteredTaskList(state) {
            return state.filteredTaskList;
        },
        getFilteringMode(state) {
            return state.isFilteringMode;
        },
    },
    actions: {
        logout({ commit }) {
            commit("RESET_STATE");
        },
    },
});

function getDefaultState() {
    return {
        workspaces: [],
        tables: [],
        sections: [],
        tasks: [],
        selectedWorkspace: {},
        selectedWorkspaceObj: {},
        selectedTable: {},
        tasksLoading: false,
        activeSection: 0,
        isTaskEditingMode: false,
        editingTask: null,
        searchString: "",
        filteredTaskList: [],
        isFilteringMode: false,
    };
}

// Vuetify

const vuetify = createVuetify({
    components,
    directives,
    ssr: true,
});

createInertiaApp({
    resolve: (name) =>
        resolvePageComponent(
            `./Pages/${name}.vue`,
            import.meta.glob("./Pages/**/*.vue")
        ),
    setup({ el, App, props, plugin }) {
        return createApp({ render: () => h(App, props) })
            .use(plugin)
            .component("font-awesome-icon", FontAwesomeIcon)
            .use(VueAxios, axios)
            .use(store)
            .use(vuetify)
            .use(VueGanttastic)
            .component("Favicon", Favicon)
            .component("inertia-link", Link)
            .mount(el);
    },
});
