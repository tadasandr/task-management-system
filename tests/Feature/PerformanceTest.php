<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

/* 
* @test
* @large
*/
class PerformanceTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_performance()
    {
        $requests = 50;
        $startTime = microtime(true);
        for ($i = 0; $i < $requests; $i++) {
            $response = $this->get('/authentication');
            $response->assertStatus(200);
        }
        $elapsedTime = microtime(true) - $startTime;
        $threshold = 2.5;
        $this->assertLessThan($threshold, $elapsedTime, "Elapsed time is greater than the threshold");
    }
}
