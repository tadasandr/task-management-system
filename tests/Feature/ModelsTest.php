<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\Workspace;
use App\Models\Table;
use App\Models\User;
use App\Models\Section;
use App\Modelss\Task;
use Tests\TestCase;

class ModelsTest extends TestCase
{
    public function test_workspace_model() {
        $workspace = new \App\Models\Workspace();
        $this->assertNotNull($workspace);
    }

    public function test_table_model() {
        $table = new \App\Models\Table();
        $this->assertNotNull($table);
    }

    public function test_table_belongs_to_workspace()
{
    $table = Table::factory()->create();
    $this->assertInstanceOf(Workspace::class, $table->workspace);
}

public function test_table_has_many_sections()
{
    $table = Table::factory()->withSections(3)->create();
    $this->assertCount(3, $table->sections);
    $table->sections->each(function ($section) use ($table) {
        $this->assertEquals($table->id, $section->table_id);
    });
}

public function test_section_has_many_tasks()
{
    $section = Section::factory()->withTasks(3)->create();
    $this->assertCount(3, $section->tasks);
    $section->tasks->each(function ($task) use ($section) {
        $this->assertEquals($section->id, $task->section_id);
    });
}
}
