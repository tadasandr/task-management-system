<?php

namespace Tests\Feature;

use App\Models\User;
use DateTime;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ParameterizedTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @dataProvider userIsActiveDataProvider
     * @param string $accountStatus
     * @param string $lastLoginDate
     * @param string $currentDate
     * @param int $daysThreshold
     * @param bool $expected
     */
    public function test_user_is_active(
        string $accountStatus,
        string $lastLoginDate,
        string $currentDate,
        int $daysThreshold,
        bool $expected
    ): void {
        $user = User::create([
            'account_status' => $accountStatus,
            'last_login_date' => new DateTime($lastLoginDate),
            'name' => "Tadas Andrijauskas",
            'email' => "tadasandrijauskass@gmail.com",
            'avatar_color' => "#fb0d5f",
            'password' => "tadas123"
        ]);

        $isActive = $user->isActive(new \DateTime($currentDate), $daysThreshold);

        $this->assertEquals($expected, $isActive);
    }

    public function userIsActiveDataProvider(): array
    {
        return [
            ['inactive', '2023-01-01', '2023-02-01', 30, false],
            ['active', '2023-01-01', '2023-01-15', 30, true],
            ['active', '2022-12-01', '2023-02-01', 30, false],
            ['inactive', '2022-12-01', '2023-02-01', 30, false],
            ['active', '2023-01-15', '2023-02-12', 30, true],
        ];
    }
}