<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Table;
use App\Models\Section;
use App\Models\Workspace;

class WorkspaceControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_workspace_controller_add_username_to_workspace_method() {
        $user = User::factory()->create();
        $workspace = Workspace::factory()->create();
        $workspace->users()->attach($user->id);
        $this->assertDatabaseHas('user_workspace', [
            'user_id' => $user->id,
            'workspace_id' => $workspace->id
        ]);
    }

    public function test_workspace_controller_remove_username_from_workspace_method() {
        $user = User::factory()->create();
        $workspace = Workspace::factory()->create();
        $workspace->users()->attach($user->id);
        $workspace->users()->detach($user->id);
        $this->assertDatabaseMissing('user_workspace', [
            'user_id' => $user->id,
            'workspace_id' => $workspace->id
        ]);
    }
}
