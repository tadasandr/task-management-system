<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;
use App\Models\Section;
use App\Models\Workspace;
use App\Models\Table;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SectionControllerTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */

    use RefreshDatabase;

    public function test_section_controller_store_method() {
        $user = User::factory()->create();
        $workspace = Workspace::factory()->create();
        $workspace->users()->attach($user->id);
        $table = Table::factory()->create();
        $table->workspace_id = $workspace->id;
        $table->save();
        $section = Section::factory()->create();
        $section->table_id = $table->id;
        $section->save();
        $this->assertDatabaseHas('sections', [
            'id' => $section->id,
            'table_id' => $table->id,
            'name' => $section->name,
            'created_at' => $section->created_at,
            'updated_at' => $section->updated_at,
        ]);
    }

    
}
