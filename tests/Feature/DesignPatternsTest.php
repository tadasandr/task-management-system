<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DesignPatternsTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_console_command_total_users_command() {
        $this->artisan('user:count')
             ->expectsOutput('There are 0 users in the database.')
             ->assertExitCode(0);
    }

    public function test_email_already_exists_exception() {
        $this->expectException(\App\Exceptions\EmailAlreadyExistsException::class);
        throw new \App\Exceptions\EmailAlreadyExistsException();
    }
}
