<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Workspace;
use App\Models\Table;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Inertia\Testing\AssertableInertia as Assert;
use Tests\TestCase;
use Inertia\Inertia;

// @test
class DashboardControllerTest extends TestCase
{
    use RefreshDatabase;
    public function test_dashboard_index()
    {
        $user = User::factory()->create();
        $workspace = Workspace::factory()->create(['owner' => $user->id]);
    
        $response = $this->actingAs($user)->get('/dashboard');
    
        $response->assertStatus(200);
        $response->assertInertia(function (Assert $page) use ($workspace) {
            $page->component('Dashboard');
            $page->has('workspaces', 1);
            $page->where('workspaces.0.id', $workspace->id);
            $page->where('workspaces.0.name', $workspace->name);
        });
    }

    public function test_redirect_if_authenticated() {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->get('/dashboard');
        $response->assertStatus(200);
    }
}