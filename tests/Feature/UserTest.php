<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    use RefreshDatabase;

    public function test_user_registration_successful()
    {
        $response = $this->post('/register', [
            'name' => 'John Doe',
            'email' => 'john@example.com',
            'password' => 'password',
        ]);
        $response->assertRedirect('/dashboard');
        $this->assertAuthenticatedAs(User::where('email', 'john@example.com')->first());
    }

    /**
     * Exception testing
     * @expectedException
     * @return void
     */
    public function testEmailAlreadyExists()
    {
        $user = User::factory()->create();
            $response = $this->postJson('/register', [
            'name' => 'Tadas Andrijauskas',
            'email' => $user->email,
            'password' => 'password',
        ]);
            $response->assertStatus(500);
            $this->expectException(\Illuminate\Database\QueryException::class);
            User::factory()->create(['email' => $user->email]);
    }
}
