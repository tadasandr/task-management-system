<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MiddlewareTest extends TestCase
{
    public function test_http_middleware_authenticate() {
        $response = $this->get('/dashboard');
        $response->assertStatus(302);
    }
}
